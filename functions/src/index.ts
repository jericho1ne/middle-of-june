//
// Check build script suggestions:
// https://firebase.google.com/docs/functions/typescript
//

// Firebase required functions
const functions = require('firebase-functions')

// Packages needed to make things happen
const express = require('express')
const cors = require('cors')
const https = require('https')

// Get a server up and running
// Use same default port as Firebase for simplicity
let app = express()

// Get past CORS issues
app.use(cors())

/**
 * :test
 *
 * Make sure Twitter API auth works
 * URL example: http://localhost:3000/test/
 * @return Either an error, or a valid response containing current user's info
 */
app.get('/test/', (req, res) => {
  console.log(`*** testing w/ ${req.params}`)

  res.send({
    "data": "testing 1, 2, 3, 4"
  })
})
/**
 * Get shows from a specific Venue
 * URL example: http://localhost:<port>/name/echoplex
 */
app.get('/venue/:name', (req, res) => {
  console.log(`*** get info on: ${req.params.name}`)

  res.send({
    "venue": req.params.name,
    "data": "thing",
  })
})
app.get('/shows/', (req, res) => {
  const customParams = [
    'startDate',
    'venue.id',
    'venue.name',
    'venue.address1',
    'ageLimit',
    'headlinersName',
    'supportsName',
    'headliners.image.large',
    'headliners.eventDescription',

    // Visual stuff
    'image.original',
    'image.large',
    'image.square',

    // If a paid event
    'ticketPurchaseUrl',

    // If a free event, fall back upon this, it'll always be there
    'urlEventDetailsUrl',
    'ticketPrice'
  ]

  const maxEvents = 30;
  const ticketflyUrl =
    `https://www.ticketfly.com/api/events/upcoming.json?` +
    `orgId=1&city=Los%20Angeles` +
    `&maxResults=${maxEvents}` +
    `&fields=${customParams.join(',')}`

  console.log(ticketflyUrl)

  https.get(ticketflyUrl, (resp) => {
    let data = '';

    resp.on('data', (chunk) => {
      data += chunk
    })
    resp.on('end', () => {
      res.send(JSON.parse(data))
    })
  }).on("error", (err) => {
    console.log("Error: " + err.message);
  })
})
.listen();

console.log("*** Server running ***");

// Run the damn thing
exports.app = functions.https.onRequest(app);
