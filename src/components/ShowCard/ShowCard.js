import React from 'react'
// import Tag from '../Tag/Tag'
// import Multimedia from '../Multimedia/Multimedia'

import './ShowCard.scss'

const Project = ( props ) => {
  // // Get list of tech used in project, turn it into individual Tags
  // const tags = props.tech.split(",").map(function(item) {
  //   return item.trim()
  // })

  // Built list of Tag components to be displayed
  // const tagsList = tags.map(function(tag, index) {
  //   return (
  //     <Tag
  //       key={ index }
  //       name={ tag }
  //     />
  //   )
  // })

  const clickHandler = (e) => {
    console.warn('smthing was clicked, yea?')
  }

  return (
    <div className="show-card">
      <div className="card-img-top" onClick={ clickHandler }>
        <img className=""
          src={ props.images.square.path }
          alt={ props.headliner }
        />
      </div>
      <div className="card-details">
        <h3 className="card-title">{ props.headliner }</h3>
        <h4 className="">{ props.venue.name }</h4>
        <p> card details here </p>
      </div>

    </div>
  )
}

export default Project
