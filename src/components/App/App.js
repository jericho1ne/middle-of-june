import React from 'react'

import logo from '../../assets/middle-of-june-logo.svg';
import './App.scss';

import { Route, Switch, Link } from 'react-router-dom'

import Home from '../Home/Home'
import Calendar from '../Calendar/Calendar'

function App() {
  return (
    <div className="App">
      <header className="header">
        <div className="main-logo">
          <img src={logo} alt="Middle of June logo" />
        </div>
        <div className="site-title">
          <h1 >Middle Of June</h1>
        </div>
        <div className="menu">
          <Link to="/" className="button">HOME</Link>
          <Link to="/calendar" className="button">Calendar</Link>
        </div>
      </header>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/calendar" component={Calendar} />
      </Switch>
    </div>
  );
}

export default App;
