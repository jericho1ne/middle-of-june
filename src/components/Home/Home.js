import React, { Component } from 'react'
import axios from 'axios'

import ErrorBoundary from '../ErrorBoundary/ErrorBoundary'
import ShowCard from '../ShowCard/ShowCard'

import './Home.scss';

const API_BASE_URL = (process.env.NODE_ENV === 'development')
  ? process.env.REACT_APP_LOCAL_API_URL
  : process.env.REACT_APP_FIREBASE_URL

console.log(`>>>>> API URL: ${API_BASE_URL}`)

class Home extends Component {
  // Setting initial state
  state = {
    shows: [],
    loading: true,
  }

  escTriggered = (event) => {
    if (event.keyCode === 27 && this.state.projectInFocus !== '') {
      // this.setState({ projectInFocus: '' }, () => { })
    }
  }

  // Get list of current shows / events
  getShows = async () => {
    // Query the ticketfly api using Axios
    // https://malcoded.com/posts/react-http-requests-axios/
    // TODO: consider using Fetch API
    return await axios(`${API_BASE_URL}/shows/`)
  }

  /**
   *  Attach listeners when App component is first mounted
   */
  componentDidMount() {

    // Invoke the async function
    this.getShows().then((response) => {
      // console.log(response.data.events)

      this.setState({
        shows: response.data.events
      }, () => {
        console.log(response.data.events)
      })
    })

    // Attach event listeners
    document.addEventListener('keydown', this.escTriggered, false)
    document.addEventListener('scroll', this.handleScroll, { passive: true })
  }

  /**
   * Remove listeners
   */
  componentWillUnmount() {
    // Detach event listeners
    document.addEventListener('keydown', this.escTriggered, false)
    document.removeEventListener('scroll', this.handleScroll)
  }

  render() {
    const showsList = this.state.shows.map((show, index) => {
        return (
          <ErrorBoundary key={`moj-${index}`}>
            <ShowCard
              key={ index }
              venue={ show.venue }
              headliner={ show.headlinersName }
              price={ show.ticketPrice }
              purchase_url={ show.ticketPurchaseUrl }
              detail={ show.headliners }
              images={ show.image }
              date={ show.startDate  }
            />
          </ErrorBoundary>
        )
      })

    return (
      <div className="Home">
        <h1 className="site-title">Daily Show View</h1>
        <div className="shows-list">
          { showsList }
        </div>
      </div>
    )

  }
}

export default Home;
